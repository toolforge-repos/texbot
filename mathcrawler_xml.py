#!/usr/bin/env python
# -*- coding: utf-8 -*-
from lxml import etree as et
from bz2file import BZ2File
import re
from itertools import chain
import time
import sys


dbname=sys.argv[1].split('/')[-1]
inputpath='/public/dumps/public/'+dbname+'/latest/'+dbname+'-latest-pages-meta-current.xml.bz2'
outputfilename=sys.argv[1]+'_math.txt'
outputfile=open(outputfilename,'w')

begin=time.time()
pagecount=0
mathcount=0
chemcount=0
cache=[]
pagetitle=""

with BZ2File(inputpath) as xml_file:
	parser = et.iterparse(xml_file, events=('end',))
	for events, elem in parser:
		if et.QName(elem.tag).localname == "title":
			pagetitle=elem.text
			pagecount+=1

			if pagecount%1000 == 0:
				print str(pagecount)+' pages in '+str(time.time()-begin)+' seconds'
				sys.stdout.flush()
		if et.QName(elem.tag).localname == "text" and elem.text:
			content=elem.text

			for math in re.finditer(r'(\<math.*?\>)(.*?)(\<\/math\>)', content, re.DOTALL | re.IGNORECASE):
				mathcount+=1
				cache.append('[['+pagetitle+']] '+math.group(2).replace('\n',' ')+'\n')
			for chem in chain(re.finditer(r'(\<ce.*?\>)(.*?)(\<\/ce\>)', content, re.DOTALL | re.IGNORECASE),re.finditer(r'(\<chem.*?\>)(.*?)(\<\/chem\>)', content, re.DOTALL | re.IGNORECASE)):
				chemcount+=1
				cache.append('[['+pagetitle+']] \ce{'+chem.group(2).replace('\n',' ')+'}'+'\n')
			if cache:
				outputfile.write(u''.join(cache).encode('utf-8'))
				cache=[]
		elem.clear()
		while elem.getprevious() is not None:
			del elem.getparent()[0]


print 'time:  '+str(time.time()-begin)+' seconds'
print 'pages: '+str(pagecount)
print 'math:  '+str(mathcount)
print 'chem:  '+str(chemcount)

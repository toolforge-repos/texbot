#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pywikibot
import re
from pywikibot import textlib
from pywikibot import pagegenerators
from itertools import chain
import time
import sys


dbname=sys.argv[1].split('/')[-1]
outputfilename=sys.argv[1]+'_math.txt'
outputfile=open(outputfilename,'w')
site=pywikibot.Site(pywikibot.site.APISite.fromDBName(dbname).code, pywikibot.site.APISite.fromDBName(dbname).fam())

begin=time.time()
pagecount=0
mathcount=0
chemcount=0
cache=[]

#for page in site.preloadpages(pagegenerators.AllpagesPageGenerator(site=site, includeredirects=False)): #expensive API pagegenerator
for page in pagegenerators.XMLDumpOldPageGenerator(filename='/public/dumps/public/'+dbname+'/latest/'+dbname+'-latest-pages-meta-current.xml.bz2',site=site):
	pagecount+=1

	content=page.text
	content=textlib.removeDisabledParts(content)
	content=re.sub(r'(\<math.*?\>)(.*?)(\<\/math\>)',"",content,flags=re.DOTALL | re.IGNORECASE)
	content=re.sub(r'(\<chem.*?\>)(.*?)(\<\/chem\>)',"",content,flags=re.DOTALL | re.IGNORECASE)
	content=re.sub(r'(\<ce.*?\>)(.*?)(\<\/ce\>)',"",content,flags=re.DOTALL | re.IGNORECASE)

	content=site.expand_text(content) #expensive API call
	for math in re.finditer(r'(\<math.*?\>)(.*?)(\<\/math\>)', content, re.DOTALL | re.IGNORECASE):
		mathcount+=1
		cache.append('[['+page.title()+']] '+math.group(2).replace('\n',' ')+'\n')
	for chem in chain(re.finditer(r'(\<ce.*?\>)(.*?)(\<\/ce\>)', content, re.DOTALL | re.IGNORECASE),re.finditer(r'(\<chem.*?\>)(.*?)(\<\/chem\>)', content, re.DOTALL | re.IGNORECASE)):
		chemcount+=1
		cache.append('[['+page.title()+']] \ce{'+chem.group(2).replace('\n',' ')+'}'+'\n')
	if cache:
		outputfile.write(u''.join(cache).encode('utf-8'))
		cache=[]

	if pagecount%1000 == 0:
		print str(pagecount)+' pages in '+str(time.time()-begin)+' seconds'
		sys.stdout.flush()
	if pagecount%100000 == 0:
		print "breaking"
		break

print 'time:  '+str(time.time()-begin)+' seconds'
print 'pages: '+str(pagecount)
print 'math:  '+str(mathcount)
print 'chem:  '+str(chemcount)

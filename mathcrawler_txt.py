#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
from xml.sax.saxutils import escape, unescape
from bz2file import BZ2File
import time

html_escape_table = {
    '"': "&quot;",
    "'": "&apos;"
}
html_unescape_table = {v:k for k, v in html_escape_table.items()}

def html_escape(text):
    return escape(text, html_escape_table)

def html_unescape(text):
    return unescape(text, html_unescape_table)

titleRE = re.compile('<title>(.*?)</title>')
mathRE = re.compile(r"&lt;(/?)(math|chem|ce)\b(.*?)&gt;",re.IGNORECASE | re.DOTALL)


dbname=sys.argv[1].split('/')[-1]
inputpath='/public/dumps/public/'+dbname+'/latest/'+dbname+'-latest-pages-meta-current.xml.bz2'
outputfilename=sys.argv[1]+'_math.txt'
outputfile=open(outputfilename,'w')

begin=time.time()
pagecount=0
mathcount=0
chemcount=0
cache=[]

title =""
inEqn = False
isXML = True

for line in BZ2File(inputpath, "r"):
    if isXML:
	m = titleRE.search(line)
	if m :
		title = m.group(1)
		expression = ""
		inEqn = False
		pagecount += 1
		if pagecount%1000 == 0:
			print str(pagecount)+' pages in '+str(time.time()-begin)+' seconds'
			sys.stdout.flush()
		continue

	elif line == '      <format>text/x-wiki</format>\n': #line before text
		isXML = False

    else :
	if line == '  </page>\n':
		isXML = True
		if cache:
			outputfile.write(''.join(cache))
			cache=[]
		continue
	start = 0
	pos = 0
	m = mathRE.search(line,pos)
	while m :
		if m.group(1): 
			end = m.start()
			expression = ''.join([expression,line[start:end]])
			if not 'math' in m.group(2).lower():
				chemcount += 1
				cache.append('\n[[%s]] \ce{%s}' % (title, html_unescape(expression).replace('\n',' ')))
			else:
				mathcount += 1
				cache.append('\n[[%s]] %s' % (title, html_unescape(expression).replace('\n',' ')))
			pos = m.end()
			expression = ""
			start = 0
			inEqn = False
                else:
			start = m.end()
			pos = start
			expression = ""
			inEqn = True
		m = mathRE.search(line,pos)
	if start > 0 :
		expression = line[start:]
	elif inEqn :
		expression = ''.join([expression,line])

outputfile.write(''.join(cache))
print 'time:  '+str(time.time()-begin)+' seconds'
print 'pages: '+str(pagecount)
print 'math:  '+str(mathcount)
print 'chem:  '+str(chemcount)

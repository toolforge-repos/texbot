#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pywikibot
import re
import sys
import time
import mwparserfromhell
import requests
from itertools import izip_longest

reload(sys)
sys.setdefaultencoding('utf8')

mathregex=re.compile(r'(\<math\>|\<math\s.*\>)(.*?)(\<\/math\s*?\>)', flags=re.DOTALL | re.IGNORECASE)

nowikipatterns=re.compile(r'(\<nowiki.*?\>.*?\<\/nowiki\>)|(\<pre.*?\>.*?\<\/pre\>)|(\<source.*?\>.*?\<\/source\>)|(\<syntaxhighlight.*?\>.*?\<\/syntaxhighlight\>)|(\<!--.*?--\>)', flags=re.DOTALL | re.IGNORECASE)

def replacemacros(mstr):
	mstr=re.sub(r'(?<!\\)\\or(?![a-zA-Z])', u'\\lor',mstr)
	mstr=re.sub(r'(?<!\\)\\and(?![a-zA-Z])', u'\\land',mstr)
	mstr=re.sub(r'(?<!\\)\$', u'\\$',mstr)
	mstr=re.sub(r'(?<!\\)%', u'\\%',mstr)
	mstr=re.sub(r'(?<!\\)\\part(?![a-zA-Z])', u'\\partial',mstr)
	mstr=re.sub(r'(?<!\\)\\ang(?![a-zA-Z])', r'\\angle',mstr) # don't want u'\\angle' here as some wierd unicode error
	mstr=re.sub(r'(?<!\\)\\C(?![a-zA-Z])', u'\\Complex',mstr)
	mstr=re.sub(r'(?<!\\)\\H(?![a-zA-Z])', u'\\mathbb{H}',mstr)
	mstr=re.sub(r'(?<!\\)\\bold(?![a-zA-Z])', u'\\mathbf',mstr)
	mstr=re.sub(r'(?<!\\)\\Bbb(?![a-zA-Z])', u'\\mathbb',mstr)
	return mstr

def comparetemplate(page, oldtemplate, newtemplate):
	# returns true if rendering of templates is equal
	oldcontent=site.expand_text(unicode(mwparserfromhell.parse(oldtemplate)),title=page.title())
	newcontent=site.expand_text(unicode(mwparserfromhell.parse(newtemplate)),title=page.title())
	oldtext=re.sub(mathregex, u'', oldcontent, count=0)
	newtext=re.sub(mathregex, u'', newcontent, count=0)
	if not (oldtext == newtext):
		return False # template outside of math is different
	oldmath=re.finditer(mathregex, oldcontent)
	newmath=re.finditer(mathregex, newcontent)
	sentinel="this is compared if a template is missing a math tag"
	for oldm, newm in izip_longest(oldmath, newmath, fillvalue=sentinel):
		oldres = requests.post('https://en.wikipedia.org/api/rest_v1/media/math/check/chem', data={'q' : oldm.group(2)})
		newres = requests.post('https://en.wikipedia.org/api/rest_v1/media/math/check/chem', data={'q' : newm.group(2)})
		if not (oldres.status_code == newres.status_code == 200):
			return False # some math did not render
		oldimg = requests.get('https://en.wikipedia.org/api/rest_v1/media/math/render/svg/'+oldres.headers['x-resource-location'])
		newimg = requests.get('https://en.wikipedia.org/api/rest_v1/media/math/render/svg/'+newres.headers['x-resource-location'])
		oldsvg = re.sub(r'<title.*title>', u'', oldimg.text) # delete title containing latex source
		newsvg = re.sub(r'<title.*title>', u'', newimg.text)
		if not (oldsvg == newsvg):
			return False # svg is different
	return True


inputfilename = 'inputlist_en.txt'
if len(sys.argv) > 1:
	inputfilename = sys.argv[1]
ignorefilename = 'ignorelist.txt'

with open(inputfilename) as inputfile:
	lines = inputfile.readlines()
lines = [x.strip() for x in lines] # remove \n

with open(ignorefilename) as ignorefile:
	ignorelines = ignorefile.readlines()
ignorelines = [x.strip() for x in ignorelines] # remove \n

for line in lines: #for each line "databasename articlename" in input list
	if line in ignorelines:
		print line + " is on ignore list"
		continue
	dbname, pagename=line.split(" ",1)
	try:
		site=pywikibot.Site(pywikibot.site.APISite.fromDBName(dbname).code, pywikibot.site.APISite.fromDBName(dbname).fam())
	except pywikibot.exceptions.UnknownFamily as e:
		print line + " not known: " + str(e)
		continue
	page=pywikibot.Page(site,pagename.decode('utf-8'))
	changed=False
	reject=False
	print("Trying page ",pagename)

	nowikiloc=[]
	for nowiki in re.finditer(nowikipatterns,page.text):
		nowikiloc.append([nowiki.start(),nowiki.end()])
	if len(nowikiloc) > 0:
		print "nowiki tags detected"
	for math in sorted(re.finditer(mathregex, page.text), reverse=True, key=lambda x : x.start()): #get all math tags in reverse order such that the math.start() and math.end() positions can be used
		replace=True #only replace if this flag stays == True
		for nw in nowikiloc: 
			if nw[0] < math.start() and math.end() < nw[1]:
				print "complete math formula is inside nowiki"
#				replace=False #complete math formula is inside nowiki
#				reject=True
#				break
			elif nw[0] < math.start() < nw[1] or nw[0] < math.end() < nw[1]:
				print "one of the math tags is inside nowiki"
#				replace=False #one of the math tags is inside nowiki
#				reject=True
#				break
		mstr=replacemacros(math.group(2))
		if mstr == math.group(2):
			replace=False #nothing to replace
		if replace:
			page.text=page.text[:math.start()] + math.group(1) + mstr + math.group(3) + page.text[math.end():]
			changed=True
	#replace content of math tags hidden in templates:
	code=mwparserfromhell.parse(page.text)
	for oldtemplate in code.filter_templates():
		if(dbname=="dewikiversity"): #try special replacenemts with \C -> {{C}}
			newtemplate=mwparserfromhell.parse(replacemacros(unicode(oldtemplate).replace(u"\C",u"{{CC}}")))
			if not(oldtemplate==newtemplate):
				if(comparetemplate(page,oldtemplate,newtemplate)):
					page.text=page.text.replace(unicode(oldtemplate),unicode(newtemplate))
					changed=True
					continue
		newtemplate=mwparserfromhell.parse(replacemacros(unicode(oldtemplate))) #normal replacements with \C -> \Complex
		if not(oldtemplate==newtemplate):
			if(comparetemplate(page,oldtemplate,newtemplate)):
				page.text=page.text.replace(unicode(oldtemplate),unicode(newtemplate))
				changed=True
			else:
				print line + " template " + oldtemplate.name.strip() + " not replaced, rendering is different"
	if reject:
		print line + " not saved, rejected due to nowiki tags"
	elif not changed:
		print line + " not saved, no changes"
	elif page.exists() and page.text: #don't create empty pages in case the listed article doesn't exist
		editmessage=u"Replacing deprecated latex syntax [[mw:Extension:Math/Roadmap]]"
		if(site.lang=="de"):
			editmessage=u"Texvc Makros durch LaTeX Pendant ersetzt gemäß [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="fr"):
			editmessage=u"Bot: Mise à jour des codes texvc par des équivalentes LaTeX ([[mw:Extension:Math/Roadmap|documentation]])"
		elif(site.lang=="it"):
			editmessage=u"Correggo sintassi in formula matematica secondo [[mw:Extension:Math/Roadmap]]"
			time.sleep(40)
		elif(site.lang=="es"):
			editmessage=u"Bot: reemplazando sintaxis obsoleta de Látex, véase [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="ru"):
			editmessage=u"Бот: замена устаревшего математического синтаксиса в соответствии с [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="pl"):
			editmessage=u"Zastępowanie przestarzałej składni LaTeX zgodnie z [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="sv"):
			editmessage=u"Ersätter föråldrad LaTeX-syntax enligt [[mw:Extension:Math/Roadmap]]"
			time.sleep(10)
		elif(site.lang=="nl"):
			editmessage=u"Bot: corrigeren verouderde syntax in formules - [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="pt"):
			editmessage=u"[[WP:BOT]]: Substituindo sintaxe matemática obsoleta de acordo com [[mw:Extension:Math/Roadmap]]"
			time.sleep(10)
		elif(site.lang=="ar"):
			editmessage=u"استبدل تركيب الرياضيات المقطعي حسب [[mw:Extension:Math/Roadmap|هنا]]"
		elif(site.lang=="he"):
			editmessage=u"החלפת קוד LaTeX מיושן [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="zh"):
			editmessage=u"[[mw:Extension:Math/Roadmap|替换弃用的数学语法]]"
		elif(site.lang=="ro"):
			editmessage=u"Înlocuirea sintaxei LaTeX învechite, conform [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="bg"):
			editmessage=u"Замяна на оттегления синтекс на латекс в съответствие с [[mw:Extensions:Math/Roadmap]]"
		elif(site.lang=="da"):
			editmessage=u"Udskiftning af udskrevet latexsyntax ifølge [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="cs"):
			editmessage=u"Robot: náhrada zastaralé matematické syntaxe podle [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="fa"):
			editmessage=u"جایگزینی نحو نادرست ریاضی با توجه به [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="vi"):
			editmessage=u"Thay thế công thức toán đã cũ bằng công thức mới theo [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="id"):
			editmessage=u"ganti sintaks matematika yang sudah usang sesuai dengan mw:Extension:Math/Roadmap"
		elif(site.lang=="el"):
			editmessage=u"Αντικατάσταση παρωχημένης σύνταξης latex ([[mw:Extension:Math/Roadmap]])"
		elif(site.lang=="hu"):
			editmessage=u"Elavult matematikai szintaxis cseréje [[mw:Extension:Math/Roadmap]] alapján"
		elif(site.lang=="sr"):
			editmessage=u"Заменa застареле математичке синтаксе према [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="ko"):
			editmessage=u"[[mw:Extension:Math/Roadmap]]에 따라 사용되지 않는 수학 구문을 대체함"
		elif(site.lang=="fi"):
			editmessage=u"Botti päivitti vanhentuneen matemaattisen syntaksin; ks. [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="gl"):
			editmessage=u"Substitúe a sintaxe de matemática obsoleto segundo [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="tr"):
			editmessage=u"[[mw:Extension:Math/Roadmap|Matematik sözdizimi hatası]] düzeltildi"
		elif(site.lang=="ja"):
			editmessage=u"廃止された数式構文を[[mw:Extension:Math/Roadmap]]に従って置き換える"
			time.sleep(10)
		elif(site.lang=="mk"):
			editmessage=u"Замена на застарена математичка синтакса согласно [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="uk"):
			editmessage=u"Заміна застарілого математичного синтаксису відповідно до [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="th"):
			editmessage=u"แทนที่ไวยากรณ์คณิตศาสตร์ที่เลิกใช้แล้วตาม [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="nb"): #language code on nowiki
			editmessage=u"Erstatter utdatert matematisk syntaks, se [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="hr"):
			editmessage=u"Zamijenite zastarjelu matematičku sintaksu prema [[mw:Extension:Math/Roadmap|kažiputu]]"
		elif(site.lang=="ca"):
			editmessage=u"Substitueix la sintaxi de matemàtiques obsoletes d'acord amb [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="eo"):
			editmessage=u"Anstataŭigi senvalorigitan matematikan sintakson laŭ [[mw:Extension:Math/Roadmap]]"
		elif(site.lang=="hi"):
			editmessage=u"[[mw:Extension:Math/Roadmap|रोडमैप]] के अनुसार हटाए गए गणित सिंटैक्स को बद"
		elif(site.lang=="hy"):
			editmessage=u"Հին LaTeX շարադրանքը փոխարինում եմ նորով, տես՝ [[mw:Extension:Math/Roadmap]]"
		try:
			page.save(summary=editmessage)
		except pywikibot.exceptions.PageNotSaved as e: #page is protected or has a nobot template
			print line + " not saved: " + str(e)
			pass
	else:
		print line + " not saved: Page does not exist"
	sys.stdout.flush()

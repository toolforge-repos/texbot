#!/bin/bash

#PYTHONSCRIPT="mathcrawler_bot.py"
PYTHONSCRIPT="mathcrawler_txt.py"
#PYTHONSCRIPT="mathcrawler_hid.py"
XMLPATH="/public/dumps/public/*/latest"
XMLNAME="*-latest-pages-meta-current.xml.bz2"
#XMLNAME="dewiki-latest-pages-meta-current.xml.bz2"
OUTPUTDIR=$PWD"/../math-$(date +'%Y-%m-%d')"

mkdir $OUTPUTDIR
cp $PYTHONSCRIPT $OUTPUTDIR
cp $0 $OUTPUTDIR
cp user-config.py $OUTPUTDIR
cd $OUTPUTDIR
chmod 755 $PYTHONSCRIPT
echo collecting files with name "$XMLNAME" in "$XMLPATH"
for xmlsource in $(find $XMLPATH -name "${XMLNAME}"); do
	dbname=$(basename -- "$xmlsource")
	dbname=${dbname%%-*}
	echo creating task script for $dbname
	cat > ${dbname}_script.sh <<TASKSCRIPT
#!/bin/bash
source /data/project/texbot/.env/bin/activate
$OUTPUTDIR/$PYTHONSCRIPT $OUTPUTDIR/$dbname
TASKSCRIPT
	chmod 755 ${dbname}_script.sh
	if [[ $(qstat | wc -l) -gt 40 ]]; then
		echo jsub ${dbname}_script.sh -o $OUTPUTDIR -e $OUTPUTDIR >> launch_remaining_script.sh
	elif [[ $(qstat | wc -l) -gt 30 ]]; then
		jsub ${dbname}_script.sh -o $OUTPUTDIR -e $OUTPUTDIR
		sleep 20
	else
		jsub ${dbname}_script.sh -o $OUTPUTDIR -e $OUTPUTDIR
	fi;
done
echo script $0 finished
